﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using mysite.Models;
using System.Security;

namespace mysite.Controllers
{
    public class LoginController : Controller
    {
        DatabaseContextEntities dbce = new DatabaseContextEntities();
        
       
        [Authorize(Roles = "Admin")]
        public ActionResult AdminIndex()
        {
           
            return View();
        }

        [Authorize(Roles = "User")]
        public ActionResult UserIndex()
        {
            string writer = User.Identity.Name;
            
            ////var article = dbce.articles.FirstOrDefault(x => x.WriterArticle.Equals(s));

            // article artt = dbce.articles.Select(x => x.WriterArticle.Equals(writer));

            //// var article = dbce.articles.FirstOrDefault(u => u.Username.Equals(l.Username) && u.Password.Equals(l.Password));
          
               return View(dbce.articles.Where(x => x.WriterArticle.Equals(writer)).ToList());

            //   return View(ndb.a .OrderByDescending(e => e.DateNews).Take(3).ToList());

        }
    }
}