﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mysite.Models;

namespace mysite.Controllers
{
    [Authorize(Roles = "User")]
    public class UploadController : Controller
    {
        private DatabaseContextEntities db =new DatabaseContextEntities();
        // GET: Upload
        [HttpGet]
        public ActionResult load()
        {
            return View();
        }

        [HttpPost]
        public ActionResult load(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                string writer = User.Identity.Name;
                var fileName = Path.GetFileName(file.FileName);
                var fileExtention = Path.GetExtension(fileName).ToUpper();

                if (fileExtention != ".PDF")
                {
                    ViewBag.upload = "فقط فایل با پسوند .pdf را میتوانید آپلود کنید .";

                }

                else

                {
                    //string strContentType = file.ContentType.ToUpper();

                    //if (strContentType != "application/PDF")
                    //{
                    //    ViewBag.upload = "پسوند فایل تغییر داده شده است،امکان آپلود موجود نیست.";
                    //}

                    //var savePath = Path.Combine(Server.MapPath("~/uploads"), fileName);
                    //file.SaveAs(savePath);
                    //ViewBag.upload = "آپلود با موفقیت انجام شد .";

                    var strRootTelativePath = "~/uploads";
                     var strPath = Server.MapPath(strRootTelativePath);
                    if (Directory.Exists(strPath )== false)
                    {
                        Directory.CreateDirectory(strPath);
                    }
                    var strPathName = string.Format("{0}\\{1}", strPath, fileName);
                    file.SaveAs(strPathName);
                    ViewBag.upload = "آپلود با موفقیت انجام شد .";

                    article art = new article();
                    person per=  db.persons.First(p => p.FirsName == writer);
                    art.DateRecord = (Extention.ToPersian(DateTime.Now)).ToString();
                    art.IdStudent = per.IdPerson;
                    art.NameArticle = fileName;
                    art.WriterArticle = writer;

                    db.articles.Add(art);

                    db.SaveChanges();



                }
            }

            else
            {
                ViewBag.upload = "آپلود انجام نشد ، دوباره تلاش کنید.";

            }
            return View();
           
          //  return View(db.articles.OrderByDescending(e => e.DateRecord).ToList());
        }
    }
}