﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using mysite.Models;

namespace mysite.Controllers
{
    [Authorize(Roles = "Admin")]
    public class lettersController : Controller
    {
        private DatabaseContextEntities db = new DatabaseContextEntities();

        // GET: letters
        //public async Task<ActionResult> Index()
        //{

        //    return View(await db.letters.ToListAsync());

        //  //  return View(await db.letters.ToListAsync());

        //    // var letters = db.letters.Include(l => l.person);
        //    //  return View(await db.projects.ToListAsync());
        //    //  return View(await db.letters.ToListAsync());

        //    //  return View(await db.letters.ToListAsync());
        //}

        public async Task<ActionResult> Index()
        {

            return View(await db.letters.ToListAsync());

        }

        // GET: letters/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            letter letter = await db.letters.FindAsync(id);
            if (letter == null)
            {
                return HttpNotFound();
            }
            return View(letter);
        }

        // GET: letters/Create
        public ActionResult Create()
        {
         //   ViewBag.myperson_IdPerson = new SelectList(db.persons, "IdPerson", "FirstName");
            return View();
        }

        // POST: letters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdLetter,Confirmative,NamePart,SendDate")] letter letter)
        {
            if (ModelState.IsValid)
            {
                int max;
                if (db.letters == null)
                    max = 0;
                else
                    max = db.letters.Max(p => p.IdLetter);

                letter.IdLetter = max + 1;
                    db.letters.Add(letter);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

       //     ViewBag.myperson_IdPerson = new SelectList(db.persons, "IdPerson", "FirstName");
            return View(letter);
        }

        // GET: letters/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            letter letter = await db.letters.FindAsync(id);
            if (letter == null)
            {
                return HttpNotFound();
            }
      //      ViewBag.myperson_IdPerson = new SelectList(db.persons, "IdPerson", "FirstName");
            return View(letter);
        }

        // POST: letters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdLetter,Confirmative,NamePart,SendDate")] letter letter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(letter).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
           // ViewBag.myperson_IdPerson = new SelectList(db.persons, "IdPerson", "FirstName");
            return View(letter);
        }

        // GET: letters/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            letter letter = await db.letters.FindAsync(id);
            if (letter == null)
            {
                return HttpNotFound();
            }
            return View(letter);
        }

        // POST: letters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            letter letter = await db.letters.FindAsync(id);
            db.letters.Remove(letter);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
