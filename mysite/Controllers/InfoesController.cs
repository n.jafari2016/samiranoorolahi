﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using mysite.Models;

namespace mysite.Controllers
{
    [Authorize(Roles ="Admin")]
    public class InfoesController : Controller
    {
        private newsdbEntities db = new newsdbEntities();

        // GET: Infoes
        public async Task<ActionResult> Index()
        {
            return View(await db.Infoes.ToListAsync());
        }

      

        // GET: Infoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Infoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdInfo,InfoAssistant,InfoManager,InfoInset")] Info info)
        {
            if (ModelState.IsValid)
            {
                info.IdInfo = 1;
                db.Infoes.Add(info);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(info);
        }

        // GET: Infoes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Create");
            }

            else
            {

                Info info = await db.Infoes.FindAsync(id);
                return View(info);
            }
         
        }

        // POST: Infoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdInfo,InfoAssistant,InfoManager,InfoInset")] Info info)
        {
            if (ModelState.IsValid)
            {
                info.IdInfo = 1;
                db.Entry(info).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(info);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
