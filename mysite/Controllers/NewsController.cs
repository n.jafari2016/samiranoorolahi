﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using mysite.Models;


namespace mysite.Controllers
{
    public class NewsController : Controller
    {
        private newsdbEntities db = new newsdbEntities();
       
       
        // GET: News
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult> Index()
        {
            return View(await db.News.ToListAsync());
        }

        //برای home
        [AllowAnonymous]
        public async Task<ActionResult> ViewNews()
        {
        
            return View(await db.News.OrderByDescending(e => e.DateNews).ToListAsync());
        }


        // GET: News/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = await db.News.FindAsync(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        //baraye home
        [AllowAnonymous]
        /// baraye khabar ha ast ke jozeeat ro neshon mide
        public async Task<ActionResult> ViewDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = await db.News.FindAsync(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }


        // GET: News/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: News/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IdNews,TextNews,DateNews,TopicNews")] News news)
        {
            if (ModelState.IsValid)
            {
                int max;

                if (db.News == null)
                    max = 0;
                else
                    max = db.News.Max(p => p.IdNews) + 1;

               news.IdNews = max;

                //   news.DateNews = Extention.ToPersian(DateTime.Now);
                news.DateNews = DateTime.Now;
               // TempData["date"] = news.DateNews.ToString("yyyy/mm/dd HH:mm");

                db.News.Add(news);
                await db.SaveChangesAsync();

             //   DateTime maxdate = db.News.Max(p => p.DateNews);

             //   News onews = new News();
              //  onews = ndb.News.Where(n => n.DateNews == max).Single();
              //  onews = ndb.News.Where(n => n.DateNews == max).Single();


                return RedirectToAction("Index");
            }

            return View(news);
        }

        // GET: News/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = await db.News.FindAsync(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdNews,TextNews,DateNews,TopicNews")] News news)
        {
            if (ModelState.IsValid)
            {
                db.Entry(news).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(news);
        }


        // GET: News/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = await db.News.FindAsync(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: News/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            News news = await db.News.FindAsync(id);
            db.News.Remove(news);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

     
    }
}
