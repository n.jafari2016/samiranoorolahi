﻿using System.Linq;
using System.Web.Mvc;
using mysite.Models;
using System.Net.Mail;
using System.Net;

namespace mysite.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller

    {
        private newsdbEntities ndb = new newsdbEntities();
        private DatabaseContextEntities dbce = new DatabaseContextEntities();
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }


        
        [ChildActionOnly]
        public PartialViewResult RenderMeeting()

        {

            return PartialView(dbce.meetings.OrderByDescending(e => e.DateMeeting).Take(5));

        }

        
        [ChildActionOnly]
        public PartialViewResult RenderNews()
        {

            return PartialView(ndb.News.OrderByDescending(e => e.DateNews).Take(3));

        }


        [HttpGet]
        public ActionResult Email()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]


        //public ViewResult Email(Email _objModelMail)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        MailMessage mail = new MailMessage();
        //        mail.To.Add(_objModelMail.To);
        //        mail.From = new MailAddress(_objModelMail.From);
        //        mail.Subject = _objModelMail.Subject;
        //        string Body = _objModelMail.Body;
        //        mail.Body = Body;
        //        mail.IsBodyHtml = true;
        //        SmtpClient smtp = new SmtpClient();
        //        smtp.Host = "smtp.gmail.com";
        //        smtp.Port = 587;
        //        smtp.UseDefaultCredentials = false;
        //        smtp.Credentials = new System.Net.NetworkCredential
        //        ("username", "password");// Enter seders User name and password
        //        smtp.EnableSsl = true;
        //        smtp.Send(mail);
        //        return View("Index", _objModelMail);
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}


      //  public ActionResult Email([Bind(Include = "name,email,message")] Email email)
        public ActionResult Email(string name, string email, string message)
        {
            if (ModelState.IsValid)
            {

                string strbody = string.Empty;
                strbody = strbody + name;
                strbody = strbody + email;
                strbody = strbody + message;

                MailMessage omailmessage = new MailMessage();
                MailAddress omailaddress = null;

                omailaddress = new MailAddress("norolahis@yahoo.com",
                    "AutoResponseEmailSender",
                    System.Text.Encoding.UTF8);

                omailmessage.From = omailaddress;
                omailmessage.Sender = omailaddress;


                omailmessage.To.Clear();
                omailmessage.CC.Clear();
                omailmessage.Bcc.Clear();
                omailmessage.ReplyToList.Clear();
                omailmessage.Attachments.Clear();

                omailmessage.ReplyToList.Add(omailaddress);

                omailaddress = new MailAddress(email, name, System.Text.Encoding.UTF8);
                omailmessage.To.Add(omailaddress);

                omailmessage.BodyEncoding = System.Text.Encoding.UTF8;
                omailmessage.Body = strbody;

                omailmessage.SubjectEncoding = System.Text.Encoding.UTF8;

                omailmessage.Subject = "[-<ofogh site>-]-";

                omailmessage.IsBodyHtml = true;
                omailmessage.Priority = MailPriority.Normal;

                omailmessage.DeliveryNotificationOptions = DeliveryNotificationOptions.Never;

                //attachment

                SmtpClient osmtp = new SmtpClient();
                osmtp.Timeout = 100000;
                osmtp.EnableSsl = true;
                osmtp.Send(omailmessage);
                ViewBag.mail = "ایمیل شما با موفقیت ارسال شد.";
            }

            return View();
        }


        public ActionResult Aboutus()
        {

            return View();
        }

        public ActionResult Callus()
        {

            return View();
        }

        public ActionResult Insetpart()
        {

            return View(ndb.Infoes.ToList());
        }
        public ActionResult Manager()
        {
           
            return View(ndb.Infoes.ToList());
        }
        public ActionResult Assistant()
        {
            return View(ndb.Infoes.ToList());
        }

       

    }
}