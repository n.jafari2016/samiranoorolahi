﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using mysite.Models;

namespace mysite.Controllers
{
    public class peopleController : Controller
    {
        private DatabaseContextEntities db = new DatabaseContextEntities();
        private loginfoEntities li =new loginfoEntities();

        // GET: people
        [Authorize]
        public ActionResult Index()
        {
            return View(db.persons.ToList());
        }

        // GET: people/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            person person = db.persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: people/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            return View();
        }

        // POST: people/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPerson,Phone,FirsName,LastName,Email,Age,Password")] person person)
        {
            if (ModelState.IsValid)
            {
                if (!li.Users.Any(u => u.Username == person.FirsName.Trim().ToLower()))
                {
                    int max;
                    if (db.persons == null)
                        max = 0;
                    else
                        max = db.persons.Max(p => p.IdPerson);

                    person.IdPerson = max + 1;

                    db.persons.Add(person);
                    db.SaveChanges();

                    User user = new User();
                    UserRole ur = new UserRole();
                    user.UserID = person.IdPerson;
                    user.Username = person.FirsName;
                    user.Password = person.Password;
                    user.FirstName = person.FirsName;
                    user.LastName = person.LastName;
                    user.EmailID = person.Email;

                    ur.UserID = user.UserID;
                    ur.RoleID = 2;
                    ur.UserRolesID = user.UserID;

                    li.Users.Add(user);
                    li.UserRoles.Add(ur);
                    li.SaveChanges();

                    TempData["ok"] = "ثبت نام انجام شد.برای دیدن پروفایل خود وارد سیستم شوید.";
                    if (User.IsInRole("Admin"))
                    {

                        return RedirectToAction("Index");
                    }

                    else
                    {

                        return RedirectToAction("Login", "MyAccount");
                    }

                }

                else
                {
                    TempData["repeated"] = "نام کاربری غیرمجاز است ";
                    return View();

                }
                
            }
   return View(person);
        }


        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult CreateAdmin()
        {
            return View();

        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAdmin([Bind(Include = "IdPerson,Phone,FirsName,LastName,Email,Age,Password")] person person)
        {
            if (ModelState.IsValid)
            {

                if (! li.Users.Any(u => u.Username == person.FirsName.Trim().ToLower()))
                {
                    int max;
                    if (db.persons == null)
                        max = 0;
                    else
                        max = db.persons.Max(p => p.IdPerson);


                    person.IdPerson = max + 1;

                 

                    User user = new User();
                    UserRole ur = new UserRole();
                    user.UserID = person.IdPerson;
                    user.Username = person.FirsName;
                    user.Password = person.Password;
                    user.FirstName = person.FirsName;
                    user.LastName = person.LastName;
                    user.EmailID = person.Email;

                    ur.UserID = user.UserID;
                    ur.RoleID = 1;
                    ur.UserRolesID = user.UserID;

                    db.persons.Add(person);
                    db.SaveChanges();

                    li.Users.Add(user);
                    li.UserRoles.Add(ur);
                    li.SaveChanges();

                    return RedirectToAction("Index");

                   
                }

                else
                {
                    TempData["repeated"] = "نام کاربری غیرمجاز است ";
                    return View();
                }

            }

            return View(person);
        }



        // GET: people/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            person person = db.persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: people/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPerson,Phone,FirsName,LastName,Email,Age,Password")] person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(person);
        }


        [Authorize]
        public ActionResult EditProfile()
        {

            string name = User.Identity.Name;
            if (name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            person person = db.persons.First(a => a.FirsName == name);

            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  ActionResult EditProfile([Bind(Include = "IdPerson,Phone,FirstName,LastName,Email,Age,Password")] person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                 db.SaveChangesAsync();
                TempData["eitprofile"] = "ویرایش انجام شد";
                return RedirectToAction("UserIndex", "Login");
            }
            return View(person);
        }

        // GET: people/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            person person = db.persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: people/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            person person = db.persons.Find(id);

            User user =  li.Users.Find(id);
            UserRole role =  li.UserRoles.Find(id);

            li.Users.Remove(user);
            li.UserRoles.Remove(role);

            db.persons.Remove(person);

            db.SaveChanges();
            li.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
