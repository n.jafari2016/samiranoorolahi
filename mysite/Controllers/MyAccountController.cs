﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using mysite.Models;

namespace mysite.Controllers
{

    [AllowAnonymous]
    public class MyAccountController : Controller 
    {
        [HttpGet]
        public ActionResult Login(string ReturnUrl)
        {
            if (User.Identity.IsAuthenticated) //remember me  
            {
                if (shouldRedirect(ReturnUrl))

                {
                    return Redirect(ReturnUrl);

                }

                //return Redirect(FormsAuthentication.DefaultUrl);
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(login l, string ReturnUrl)
        {


            if (ModelState.IsValid)
            {

                loginfoEntities lf = new loginfoEntities();

                User person = lf.Users.FirstOrDefault(u => u.Username.Equals(l.Username) && u.Password.Equals(l.Password));

                if (person != null)

                {
                    FormsAuthentication.SetAuthCookie(l.Username, l.RememberMe);


                    HttpContext.Application.Lock();
                    HttpContext.Application["OnlineUsers"] = (int)HttpContext.Application["OnlineUsers"] + 1;
                    HttpContext.Application.UnLock();


                    UserRole myrole = lf.UserRoles.Find(person.UserID);
                  
                     if (myrole.RoleID == 1)
                  
                    {
                        return RedirectToAction("AdminIndex", "Login");
                    }
                     
                    else
                    {
                        return RedirectToAction("UserIndex", "Login");
                    }
                  




                }
                else
                {
                    this.ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    ViewBag.Error = "Login faild! Make sure you have entered the right user name and password!";
                    return View();

                }
            }


        ModelState.Remove("Password");
            return RedirectToAction("Errorlog", "MyAccount");

    }

        public ActionResult Errorlog()
        {
            ViewBag.Errorlog = "دوباره تلاش کنید.";
            return View("Login");
        }
        private bool shouldRedirect(string returnUrl)
        {
            return !string.IsNullOrWhiteSpace(returnUrl) &&
        Url.IsLocalUrl(returnUrl) &&
            returnUrl.Length > 1 &&
            returnUrl.StartsWith("/") &&
            !returnUrl.StartsWith("//") &&
            !returnUrl.StartsWith("/\\");
        }
        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            TempData["signout"] = "شما از سیستم خارج شدید.";

            HttpContext.Application.Lock();
            if ((int)HttpContext.Application["OnlineUsers"] <= 0)
                HttpContext.Application["OnlineUsers"] = (int)HttpContext.Application["OnlineUsers"];
            else
                HttpContext.Application["OnlineUsers"] = (int)HttpContext.Application["OnlineUsers"] - 1;

            HttpContext.Application.UnLock();



            return RedirectToAction("Login","MyAccount");
           
        }
    }


}

