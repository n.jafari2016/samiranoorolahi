﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace mysite.Models
{
    public static class Extention
    {

    
        public static DateTime ToPersian(this DateTime dt)
        {
            PersianCalendar pc = new PersianCalendar();
            int year = pc.GetYear(dt);
            int month = pc.GetMonth(dt);
            int day = pc.GetDayOfMonth(dt);
            int hour = pc.GetHour(dt);
            int min = pc.GetMinute(dt);

            DateTime persiantime = new DateTime(year, month, day, 0, 0, 0);
            return persiantime;
            //  return persiantime.ToString("yyyy/mm/dd HH:mm");

           

        }
    }
}