﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace mysite.Models
{
    public class login
    {
        public login()
        {

        }



        [Required(ErrorMessage = "نام کاربری را وارد کنید.", AllowEmptyStrings = false)]
        [Key]
        [Display(Name = "نام کاربری ")]
        public string Username
        {
            get;
            set;
        }

        [Display(Name = "رمزعبور ")]
        [Required(ErrorMessage = "رمزعبور را وارد کنید.", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password
        {
            get;
            set;
        }

        [Display(Name = "رمزعبومرا به یاد داشته باش ")]
        public bool RememberMe
        {
            get;
            set;
        }
    }
}